<?php
namespace mef\HtmlFilter;

require __DIR__ . '/../vendor/autoload.php';

$html = '
<h1>Hello, World!</h1>

<span><div>test</div></span>

<ul>
    <li>Test</li>
</ul>

<p>
    <b>This is a <b><blink>test</blink></b>
</p>
';

$htmlFilter = new HtmlFilter;

echo $htmlFilter->filter($html), PHP_EOL;

echo $htmlFilter->toText($html), PHP_EOL;