<?php

declare(strict_types=1);

namespace mef\HtmlFilter;

use mef\Http\Uri;
use DomDocument;
use DomElement;

class HtmlFilter
{
    /**
     * A list of elements that cannot contain other elements.
     */
    private const VOID_TAGS = [
        'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input',
        'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr'
    ];

    private $allowedTags = [
        'div', 'p', 'br', 'b', 'strong', 'i', 'em', 'u', 's', 'a', 'img',
        'ul', 'ol', 'li', 'span'
    ];

    /**
     * Return an HTML fragment string that has been sanitized with correct
     * syntax and "unsafe" tags removed.
     *
     * @param  string   $html   An HTML fragment.
     *
     * @return string   Safe HTMl string.
     */
    public function filter(string $html): string
    {
        $root = $this->importHtml($html);
        $html = $this->toSafeHtmlRecursion($root);

        return $html;
    }

    /**
     * Returns a text friendly version of the HTML.
     *
     * @param  string   $html   An HTML fragment
     *
     * @return string   Text only version of the HTML.
     */
    public function toText(string $html): string
    {
        $root = $this->importHtml($html);
        $text = $this->toTextRecursion($root);
        $text = trim(preg_replace('/\n{2,}/', "\n\n", $text), "\n");

        return $text;
    }

    /**
     * Load an HTML fragment into a DomDocument and return the root element.
     *
     * @param  string   $html   An HTML fragment.
     *
     * @return \DomElement Root element of the HTML.
     */
    private function importHtml(string $html): DomElement
    {
        $libXmlErrorHandling = libxml_use_internal_errors(true);
        $rootId = 'root_' . bin2hex(random_bytes(16));

        $html = '<?xml encoding="utf-8" ?><div id="' . $rootId . '">' . $html . '</div>';

        $doc = new DomDocument();
        $doc->loadHtml($html);

        libxml_use_internal_errors($libXmlErrorHandling);

        return $doc->getElementById($rootId);
    }

    /**
     * Recurse through a DOM document and sanitize it.
     *
     * @param  \DomElement   $element   The element to sanitize.
     * @param  array         $context   Optional, abstract context.
     *
     * @return string Sanitized HTML string.
     */
    private function toSafeHtmlRecursion(DomElement $element, array $context = []): string
    {
        $html = '';

        foreach ($element->childNodes as $child) {
            switch ($child->nodeType) {
                case XML_ELEMENT_NODE:
                    if (in_array($child->nodeName, $this->allowedTags)) {
                        $html .= '<' . $child->nodeName;

                        foreach ($child->attributes as $attribute) {
                            if ($attribute->name === 'src' || $attribute->name === 'href') {
                                $safeUri = $this->getSafeUri($attribute->value);
                                if ($safeUri !== '') {
                                    $html .= ' ' . $attribute->name . '="' . $this->encodeHtml($safeUri) . '"';
                                }
                            } elseif ($attribute->name === 'style') {
                                $safeStyles = $this->getSafeStyles($attribute->value);
                                if ($safeStyles !== '') {
                                    $html .= ' style="' . $this->encodeHtml($safeStyles) . '"';
                                }
                            }
                        }

                        $html .= '>';
                    } else {
                        $html .= '&lt;' . $child->nodeName . '&gt;';
                    }

                    if (in_array($child->nodeName, self::VOID_TAGS) === false) {
                        $html .= $this->toSafeHtmlRecursion($child, $context);

                        if (in_array($child->nodeName, $this->allowedTags)) {
                            $html .= '</' . $child->nodeName . '>';
                        } else {
                            $html .= '&lt;/' . $child->nodeName . '&gt;';
                        }
                    }
                    break;

                case XML_TEXT_NODE:
                    $html .= $this->encodeHtml($child->wholeText);
                    break;
            }
        }
        return $html;
    }

    /**
     * Recurse through a DOM document and convert it to text.
     *
     * @param  \DomElement   $element   The element to convert to text.
     * @param  array         $context   Optional, abstract context.
     *
     * @return string Text friendly version of the HTML.
     */
    private function toTextRecursion(DomElement $element, array $context = []): string
    {
        $text = '';
        $orderedItem = 0;

        foreach ($element->childNodes as $child) {
            switch ($child->nodeType) {
                case XML_ELEMENT_NODE:
                    if ($child->nodeName === 'div' || $child->nodeName === 'p') {
                        $text .= "\n\n";
                    } elseif ($child->nodeName === 'ul' || $child->nodeName === 'ol') {
                        $context['listDepth'] = ($context['listDepth'] ?? 0) + 1;

                        if ($context['listDepth'] === 1) {
                            $text .= "\n\n";
                        } else {
                            $text .= "\n";
                        }
                    } elseif ($child->nodeName === 'li') {
                        if ($element->nodeName === 'ul') {
                            $text .= str_repeat(' ', $context['listDepth'] * 2) . '* ';
                        } else {
                            $orderedItem++;
                            $text .= $orderedItem . '. ';
                        }
                    }

                    if (in_array($child->nodeName, self::VOID_TAGS) === false) {
                        $text .= $this->toTextRecursion($child, $context);
                    }

                    if ($child->nodeName === 'a') {
                        $href = $child->getAttribute('href');

                        if ($href !== $child->nodeValue) {
                            $text .= ' (' . $href . ')';
                        }
                    }

                    if ($child->nodeName === 'br') {
                        $text .= "\n";
                    } elseif ($child->nodeName === 'div' || $child->nodeName === 'p') {
                        $text .= "\n\n";
                    } elseif ($child->nodeName === 'li') {
                        if (substr($text, -1) !== "\n") {
                            $text .= "\n";
                        }
                    } elseif ($child->nodeName === 'ul' || $child->nodeName === 'ol') {
                        $context['listDepth']--;
                    }
                    break;

                case XML_TEXT_NODE:
                    $text .= html_entity_decode($child->wholeText, ENT_HTML5);
                    break;
            }
        }

        return $text;
    }

    /**
     * Return safe CSS styles.
     *
     * @todo This would need to be replaced with a proper CSS parser to be
     *       useful for anything other than very trivial styles.
     *
     * @param  string   $styles   The style attribute value.
     *
     * @return string Safe CSS styles.
     */
    private function getSafeStyles(string $styles): string
    {
        $rules = [];

        foreach (explode(';', $styles) as $style) {
            $style = trim($style);

            if ($style !== '') {
                list($identifier, $value) = explode(':', $style, 2);
                if ($identifier !== '') {
                    $identifier = strtolower($identifier);
                    $value = trim($value);

                    if ($identifier === 'text-align') {
                        $value = strtolower($value);
                        if (in_array($value, ['left', 'right', 'center', 'justify', 'initial', 'inherit']) === true) {
                            $rules[$identifier] = $value;
                        }
                    }
                }
            }
        }

        $styles = '';

        foreach ($rules as $identifier => $value) {
            $styles .= $identifier . ': ' . $value . '; ';
        }

        return rtrim($styles);
    }

    /**
     * Return a safe URI.
     *
     * This only returns schemeless (relative links) or http/https links.
     * It rewrites the URI to avoid triggering clever XSS attacks.
     *
     * @param  string   $uri   The URI to sanitize.
     *
     * @return string Sanitized URI.
     */
    private function getSafeUri(string $uri): string
    {
        $uri = Uri::fromString($uri);

        if (in_array($uri->getScheme(), ['', 'http', 'https', 'tel']) === true) {
            return (string) $uri;
        }

        return '';
    }

    /**
     * Encode HTML characters such that the resulting string will not trigger
     * any special meaning. (e.g., < or > characters)
     *
     * @param  string $html  The string potentially containing HTML characters.
     *
     * @return string A string containing no special HTML characters.
     */
    private function encodeHtml(string $html): string
    {
        return htmlentities($html, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);
    }
}
